# Stigmatic microscope repository

This is the stigmatic microscope repository.  It includes jupyter notebooks that can be used to analyze stigmatic microscope files and calibrate the microscope.